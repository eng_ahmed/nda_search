# Search NDA for sentences

This project searches NDA documents for certains sentences or others that carry similar meanings


### Prerequisites

- .doc/.docx file containing NDA document
- .doc/.docx file containing search strings




### Prerequisites

- Python 3.5
- genism
- GoogleNews-vectors-negative300.bin file at the same directory 

### Arguments

- -d NDA document 
- -s Search strings document


### Model
The algoritm calculates similarity between the NDA document and each strung sentnce of the given search strings after calculating their word2vec.

### Usage

```
python search.py -d 'NDA.docx' -s 'strings.docx' 
```

### Output
```
- 3 paragraphs of the NDA document with the highest probability to contain one of the search strings.
```
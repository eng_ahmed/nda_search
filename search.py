import argparse 

parser = argparse.ArgumentParser(description='document argument')
parser.add_argument('-d','--document', help='The document to be tested', required=True )
parser.add_argument('-s','--search', help='The document containging search sentences', required=False , default='NDA machine learning strings.docx' )
args = vars(parser.parse_args())
def readtxt(filename):
    import docx
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        if (para.text!=""):
            fullText.append(para.text)
        
            #print (para.text)
    #return '\n'.join(fullText)
    return fullText 




def remove_stop_words(sentence):
    from nltk.corpus import stopwords
    filtered_words = [word for word in sentence.split(" ") if word.lower() not in stopwords.words('english')]
    a=' '
    return a.join(filtered_words)



def load_model ():
    print ("loading model")
    from gensim.models.keyedvectors import KeyedVectors
    model_path = 'GoogleNews-vectors-negative300.bin'
    w2v_model = KeyedVectors.load_word2vec_format(model_path, binary=True) 
    from DocSim import DocSim
    ds = DocSim(w2v_model)
    print ("model_loaded")
    return ds

def save_as_docx(name):
    import os
    import win32com.client as win32
    from win32com.client import constants
    path1=os.getcwd()
    word = win32.gencache.EnsureDispatch('Word.Application')
    doc = word.Documents.Open(os.path.join(path1,"{0}.doc".format(name)))
    doc.Activate ()

    word.ActiveDocument.SaveAs(
        os.path.join(path1,"{0}.docx".format(name)), FileFormat=constants.wdFormatXMLDocument
    )
    doc.Close(False)


def preprocess_text(doc_name,search_name):
    doc=readtxt(doc_name)
    doc1=readtxt(search_name)
    target_doc=[remove_stop_words(item) for item in doc]
    source_docs=[remove_stop_words(item) for item in doc1]
    print ("preprocessing docs is done")
    return source_docs,target_doc,doc

def main ():
    try :
        doc_name=args['document']
        search_name=args['search']
        scores={}
        if doc_name.endswith('.doc'):
            print ("converting document into .docx")
            save_as_docx (doc_name.split('.')[0])
            doc_name=doc_name.split('.')[0]+".docx"
        if search_name.endswith('.doc'):
            print ("converting search document into .docx")
            save_as_docx (search_name.split('.')[0])
            search_name=search_name.split('.')[0]+".docx"
        
        source_docs,doc,original_doc=preprocess_text(doc_name ,search_name)
        ds=load_model ()
        
        for item in source_docs :
            sim_scores = ds.calculate_similarity(item, doc ,original_doc)
            for x in sim_scores[0:3]:
                if x['doc'] in scores.keys():
                    if x['score']>scores[x['doc']]:
                        scores[x['doc']]= x['score']
                else :
                    scores[x['doc']]= x['score']
        result=sorted(scores.items(), key=lambda x: x[1] , reverse =True)[0:3]
        print ("Paragraphs that are mostly having the ssearch query ")
        for item in result :
            print ("paragraph is : " ,item[0])
            print ("coinfidence is : " ,item[1])
    except Exception as e :
        print (e)




if __name__ == "__main__":
    main()


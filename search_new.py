#import argparse 
import traceback
import logging
#parser = argparse.ArgumentParser(description='document argument')
#parser.add_argument('-d','--document', help='The document to be tested', required=True )
#parser.add_argument('-s','--search', help='The document containging search sentences', required=False , default='NDA machine learning strings.docx' )
#args = vars(parser.parse_args())
def readtxt(filename,doc_flag):
    import docx
    from nltk.corpus import stopwords
    doc = docx.Document(filename)
    fullText = []
    try:
        header=doc.sections[0].header
        for para in header.paragraphs:
            header1=para.text
            if (header1!="" and len([word for word in header1.split(" ") if word.lower() not in stopwords.words('english') and word not in [""]])>2):
                fullText.append(header1)
    except :
        b=1
    if (doc_flag):
        for para in doc.paragraphs:
            if (para.text!=""):
                try :
                    items=para.text.split('.')
                    for item in items:
                        fullText.append(item)
                except :
                    fullText.append(para.text)
                try :
                    items=para.text.split(';')
                    for item in items:
                        fullText.append(item)
                except :
                    fullText.append(para.text)
    else :
        for para in doc.paragraphs:
            if (para.text!=""):
                fullText.append(para.text)
    if(doc_flag==True):
        if (len([word for word in fullText[0].split(" ") if word.lower() not in stopwords.words('english') and word not in [""]])<2):
            logging.warning (fullText[0])
            fullText.pop(0)
            
            logging.warning ('header deleted')
    return fullText 




def remove_stop_words(sentence):
    from nltk.corpus import stopwords
    filtered_words = [word for word in sentence.split(" ") if word.lower() not in stopwords.words('english')]
    a=' '
    return a.join(filtered_words)



def load_model (w2v_model):
    #print ("loading model")
    #from gensim.models.keyedvectors import KeyedVectors
    #model_path = 'GoogleNews-vectors-negative300.bin'
    #w2v_model = KeyedVectors.load_word2vec_format(model_path, binary=True) 
    from DocSim import DocSim
    ds = DocSim(w2v_model)
    #print ("model_loaded")
    return ds
"""
def save_as_docx(name):
    import os
    import win32com.client as win32
    from win32com.client import constants
    path1=os.getcwd()
    word = win32.gencache.EnsureDispatch('Word.Application')
    doc = word.Documents.Open(os.path.join(path1,"{0}.doc".format(name)))
    doc.Activate ()

    word.ActiveDocument.SaveAs(
        os.path.join(path1,"{0}.docx".format(name)), FileFormat=constants.wdFormatXMLDocument
    )
    doc.Close(False)
    print ("converting to .docx is done")
"""
def preprocess_text(doc_name,search_name):
    doc=readtxt(doc_name,True)
    doc1=readtxt(search_name,False)
    sections=[]
    for item,i in zip(doc1, range(len(doc1))) :
        if (item.lower().replace(" ","")=='section'):
            sections.append(i) 
    final_strings=[]
    questions=[]
    answers=[]
    for i  in range (len(sections)):
        questions.append(doc1[sections[i]+1])
        answers.append(doc1[sections[i]+2])
    for i in range (len(sections)):
        try:
            final_strings.append(doc1[sections[i]+3:sections[i+1]])
        except :
            final_strings.append(doc1[sections[i]+3:])
    target_doc=[remove_stop_words(item) for item in doc]
    target_doc=[item.lower() for item in target_doc]
    #source_docs=[remove_stop_words(item) for item in doc1]
    print ("preprocessing docs is done")
    return final_strings,target_doc,doc,questions,answers
def main (doc_name,search_name,w2v_model):
    from nltk.corpus import stopwords
    try :
        #doc_name=args['document']
        #search_name=args['search']
        scores={}
        if doc_name.endswith('.doc'):
            logging.warning  ("can't open document with format .doc only to .docx is alloed into")
            return '1','1','1',True
            #save_as_docx (doc_name.split('.')[0])
            #doc_name=doc_name.split('.')[0]+".docx"
            
        if search_name.endswith('.doc'):
            logging.warning  ("can't open document with format .doc only to .docx is alloed into ")
            return '1','1','1',True
            #save_as_docx (search_name.split('.')[0])
            #search_name=search_name.split('.')[0]+".docx"
        
        final_strings,doc,original_doc,questions,answers=preprocess_text(doc_name ,search_name)
        ds=load_model (w2v_model)
        results=[]
        answers_final=[]
        for item , item1,item2 in zip (final_strings,questions,answers) :
            scores={}
            for sentence in item :
                
                sim_scores = ds.calculate_similarity(remove_stop_words(sentence), doc ,original_doc)
                for x in sim_scores[0:3]:
                    if x['doc'] in scores.keys():
                        if x['score']>scores[x['doc']]:
                            scores[x['doc']]= x['score']
                    else :
                        scores[x['doc']]= x['score']
            final_score={}
            for k,v in scores.items():
                if (v>.84 and len([word for word in k.split(" ") if word.lower() not in stopwords.words('english') and word not in [""]])>2):
                    #print ([word for word in k.split(" ")])
                    final_score[k]=v
            if (len(final_score)==0):
                scores={}
                res=[]
                for s in original_doc :
                    result=s.split(",")
                    for itemn in result :
                        res.append(itemn)
                res_stopped=[remove_stop_words(f) for f in res]
                for sentence in item :  
                    sim_scores = ds.calculate_similarity(remove_stop_words(sentence), res_stopped ,res)
                    for x in sim_scores[0:3]:
                        if x['doc'] in scores.keys():
                            if x['score']>scores[x['doc']]:
                                scores[x['doc']]= x['score']
                        else :
                            scores[x['doc']]= x['score']
            final_score={}
            for k,v in scores.items():
                if (v>.84 and len([word for word in k.split(" ") if word.lower() not in stopwords.words('english') and word not in [""]])>2):
                    #print ([word for word in k.split(" ")])
                    final_score[k]=v
            if (len(final_score)==0):
                answers_final.append("Not found")
                #print ( sorted(final_score.items(), key=lambda x: x[1] , reverse =True)[0:3] )
            else :
                answers_final.append(item2)
            result=sorted(final_score.items(), key=lambda x: x[1] , reverse =True)[0:3]
            results.append(result)
        return questions,answers_final,results,False
    except Exception as e :
        error = traceback.format_exc()
        logging.warning ( error )

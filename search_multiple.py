

import argparse 
import traceback
parser = argparse.ArgumentParser(description='document argument')
parser.add_argument('-d','--document', help='The document to be tested', required=True )
parser.add_argument('-s','--search', help='The document containging search sentences', required=False , default='NDA machine learning strings.docx' )
args = vars(parser.parse_args())
def readtxt(filename):
    import docx
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        if (para.text!=""):
            fullText.append(para.text)
        
            #print (para.text)
    #return '\n'.join(fullText)
    return fullText 




def remove_stop_words(sentence):
    from nltk.corpus import stopwords
    filtered_words = [word for word in sentence.split(" ") if word.lower() not in stopwords.words('english')]
    a=' '
    return a.join(filtered_words)



def load_model ():
    print ("loading model")
    from gensim.models.keyedvectors import KeyedVectors
    model_path = 'GoogleNews-vectors-negative300.bin'
    w2v_model = KeyedVectors.load_word2vec_format(model_path, binary=True) 
    from DocSim import DocSim
    ds = DocSim(w2v_model)
    print ("model_loaded")
    return ds

def save_as_docx(name):
    import os
    import win32com.client as win32
    from win32com.client import constants
    path1=os.getcwd()
    word = win32.gencache.EnsureDispatch('Word.Application')
    doc = word.Documents.Open(os.path.join(path1,"{0}.doc".format(name)))
    doc.Activate ()

    word.ActiveDocument.SaveAs(
        os.path.join(path1,"{0}.docx".format(name)), FileFormat=constants.wdFormatXMLDocument
    )
    doc.Close(False)
    print ("converting to .docx is done")

def preprocess_text(doc_name,search_name):
    doc=readtxt(doc_name)
    doc1=readtxt(search_name)
    sections=[]
    for item,i in zip(doc1, range(len(doc1))) :
        if (item.lower().replace(" ","")=='section'):
            sections.append(i) 
    final_strings=[]
    questions=[]
    answers=[]
    for i  in range (len(sections)):
        questions.append(doc1[sections[i]+1])
        answers.append(doc1[sections[i]+2])
    for i in range (len(sections)):
        try:
            final_strings.append(doc1[sections[i]+3:sections[i+1]])
        except :
            final_strings.append(doc1[sections[i]+3:])
    target_doc=[remove_stop_words(item) for item in doc]
    #source_docs=[remove_stop_words(item) for item in doc1]
    print ("preprocessing docs is done")
    return final_strings,target_doc,doc,questions,answers
def main ():
    try :
        doc_name=args['document']
        search_name=args['search']
        
        if doc_name.endswith('.doc'):
            print ("converting document into .docx")
            save_as_docx (doc_name.split('.')[0])
            doc_name=doc_name.split('.')[0]+".docx"
        if search_name.endswith('.doc'):
            print ("converting search document into .docx")
            save_as_docx (search_name.split('.')[0])
            search_name=search_name.split('.')[0]+".docx"
        
        final_strings,doc,original_doc,questions,answers=preprocess_text(doc_name ,search_name)
        ds=load_model ()
        
        for item , item1,item2 in zip (final_strings,questions,answers) :
            scores={}
            for sentence in item :
                
                sim_scores = ds.calculate_similarity(remove_stop_words(sentence), doc ,original_doc)
                for x in sim_scores[0:3]:
                    if x['doc'] in scores.keys():
                        if x['score']>scores[x['doc']]:
                            scores[x['doc']]= x['score']
                    else :
                        scores[x['doc']]= x['score']
            final_score={}
            for k,v in scores.items():
                if (v>.84):
                    final_score[k]=v
            result=sorted(final_score.items(), key=lambda x: x[1] , reverse =True)[0:3]
            print (item1)
            print (item2)
            for i in range (len (result)):
                print  (result[i][0],result[i][1])
    except Exception as e :
        error = traceback.format_exc()
        print( error)




if __name__ == "__main__":
    main()



from flask import Flask, render_template, request
from flask import jsonify
import io ,os
import numpy as np
import json
import ast
import sys
app = Flask(__name__)
from flask import Flask, render_template, request ,session
from werkzeug import secure_filename
app = Flask(__name__ , template_folder=os.getcwd())
import search_new
global w2v_model
import threading
import logging
import traceback
import csv
@app.before_first_request
def activate_job():
    def run_job():
        global w2v_model
        logging.warning('loading model')
        print("Run recurring task")
        print ("loading model")
        from gensim.models.keyedvectors import KeyedVectors
        model_path = 'GoogleNews-vectors-negative300.bin'
        w2v_model= KeyedVectors.load_word2vec_format(model_path, binary=True) 
        #with app.app_context():
            #session['w2v_model'] =  KeyedVectors.load_word2vec_format(model_path, binary=True) 
        print ("model_loaded")
        logging.warning('model_loaded')
    thread = threading.Thread(target=run_job)
    thread.start()
@app.route('/')
def upload_file():
    try :
        return render_template('upload.html')
    except Exception as e :
        logging.warning(str(e))
        result_dict={"error":str(e)}
        return jsonify(result_dict)

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file1():
    global w2v_model
    try:
    #w2v_model = session.get('w2v_model', None)
        if request.method == 'POST':
            file_id = '1-jLh3Zdi-gwIFEnrw0VKQnoLH8mof2hN'
            destination = os.getcwd()+"//search_strings.docx"
            download_file_from_google_drive(file_id, destination)
            f1 = request.files['file1']
            f1.save(secure_filename(f1.filename.replace(" ", "_")))
            #f2 = request.files['file2']
            #f2.save(secure_filename(f2.filename.replace(" ", "_")))
            logging.warning (f1,w2v_model)
            questions,answers,results,flag=search_new.main(f1.filename.replace(" ", "_"),"search_strings.docx",w2v_model)
            if (flag):
                return jsonify({'error':"Documents with .doc extensions are not supported"})
            data={}
            i=1
            for item1 , item2,item3 in zip (questions,answers,results):
                data['{0}_question'.format(i).replace('"','\\"')]=str(item1).replace('"','\\"')
                data['{0}_answer'.format(i).replace('"','\\"')]=str(item2).replace('"','\\"')
                j=1
                for item in item3 :
                    data['{0}_{1}_coinfidence'.format(i,j).replace('"','\\"')]=str(item[1]).replace('"','\\"')
                    data['{0}_{1}_paragraph'.format(i,j).replace('"','\\"')]=str(item[0]).replace('"','\\"')
                    j+=1
                i+=1
            result_dict1={}
 
            for item1 , item2 in zip (questions,answers):
                result_dict1[str(item1)]=str(item2)
                
            
            msg = str(str(result_dict1).encode('utf-8'))
            with open(f1.filename.split(".docx")[0]+' results.csv', 'w') as csv_file:
                writer = csv.writer(csv_file)
                for key, value in result_dict1.items():
                    writer.writerow([key, value])
            #send_mail(f1.filename.split(".docx")[0]+' results.csv')
            #logging.warning(data)
            return render_template('index.html', data=data)
    except Exception as e :
        data={"error":str(e)}
        #send_mail_error(str(data))
        error = traceback.format_exc()
        logging.warning(error)
        return jsonify(data)

        
import requests

def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)

    if token:
        params = { 'id' : id, 'confirm' : token }
        response = session.get(URL, params = params, stream = True)

    save_response_content(response, destination)    

def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value

    return None

def send_mail(name):
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.image import MIMEImage
    import smtplib
    from email.mime.base import MIMEBase
    from email import encoders 

    fromaddr = "software_ai_hr@outlook.com"
    toaddr = ["alan.arnott@docustream.com.au","ahmed.osama.mahmud@gmail.com"]

    # instance of MIMEMultipart 
    msg = MIMEMultipart() 

    # storing the senders email address   
    msg['From'] = fromaddr 

    # storing the receivers email address  
    msg['To'] = "alan.arnott@docustream.com.au,ahmed.osama.mahmud@gmail.com"  

    # storing the subject  
    msg['Subject'] = "NDA Search"

    # string to store the body of the mail 
    body = ""

    # attach the body with the msg instance 
    msg.attach(MIMEText(body, 'plain')) 

    # open the file to be sent  
    filename = name
    attachment = open(filename, "rb") 

    # instance of MIMEBase and named as p 
    p = MIMEBase('application', 'octet-stream') 

    # To change the payload into encoded form 
    p.set_payload((attachment).read()) 

    # encode into base64 
    encoders.encode_base64(p) 

    p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 

    # attach the instance 'p' to instance 'msg' 
    msg.attach(p) 

    # creates SMTP session 
    s = smtplib.SMTP('smtp-mail.outlook.com', 587) 

    # start TLS for security 
    s.starttls() 

    # Authentication 
    s.login(fromaddr, "BrainWise2020") 

    # Converts the Multipart msg into a string 
    text = msg.as_string() 

    # sending the mail 
    s.sendmail(fromaddr, toaddr, text) 

    # terminating the session 
    s.quit() 

def send_mail_error(error):
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.image import MIMEImage
    import smtplib
    from email.mime.base import MIMEBase
    from email import encoders 

    fromaddr = "software_ai_hr@outlook.com"
    toaddr = ["alan.arnott@docustream.com.au","ahmed.osama.mahmud@gmail.com"]

    # instance of MIMEMultipart 
    msg = MIMEMultipart() 

    # storing the senders email address   
    msg['From'] = fromaddr 

    # storing the receivers email address  
    msg['To'] = "alan.arnott@docustream.com.au,ahmed.osama.mahmud@gmail.com"  

    # storing the subject  
    msg['Subject'] = "NDA Search"

    # string to store the body of the mail 
    body = error

    # attach the body with the msg instance 
    msg.attach(MIMEText(body, 'plain')) 

    # creates SMTP session 
    s = smtplib.SMTP('smtp-mail.outlook.com', 587) 

    # start TLS for security 
    s.starttls() 

    # Authentication 
    s.login(fromaddr, "BrainWise2020") 

    # Converts the Multipart msg into a string 
    text = msg.as_string() 

    # sending the mail 
    s.sendmail(fromaddr, toaddr, text) 

    # terminating the session 
    s.quit()     
def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)

if __name__ == '__main__':
    logging.warning('main')
    app.run(debug=True,host='0.0.0.0',port=8000)

